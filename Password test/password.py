import string
import random
from flask import Flask, request, jsonify

app = Flask(__name__)


@app.route('/generate_password', methods=['POST'])
def generate_password():
    length = int(request.form.get('length'))
    uppercase = bool(request.form.get('uppercase'))
    lowercase = bool(request.form.get('lowercase'))
    digits = bool(request.form.get('digits'))
    symbols = bool(request.form.get('symbols'))

    characters = ''
    if uppercase:
        characters += string.ascii_uppercase
    if lowercase:
        characters += string.ascii_lowercase
    if digits:
        characters += string.digits
    if symbols:
        characters += string.punctuation

    password = ''.join(random.choice(characters) for _ in range(length))
    return jsonify({'password': password})


if __name__ == '__main__':
    app.run(debug=True)
